#!/bin/bash
# Author:      Kaffe (simon@wagemyr.se)
# Created:     2024-06-05
# Description: New version of my "Download torrent files from rss feed"

default_config_name=$(basename "${BASH_SOURCE[0]}" .sh).yaml
script_dir=$(dirname "$(realpath "${BASH_SOURCE[0]}")")
TRD_CONFIG=${TRD_CONFIG:-$script_dir/configs/$default_config_name}

log() {
	local TRD_LOG_LEVEL=${TRD_LOG_LEVEL:-info}
	printf "%s: " "${TRD_LOG_LEVEL^^}"
	printf "%s\n" "$@"
}

warn() {
	TRD_LOG_LEVEL=warning log "$@"
}

err_exit() {
	local TRD_EXIT_CODE=${TRD_EXIT_CODE:-1}
	TRD_LOG_LEVEL=error log "$@"
	exit "$TRD_EXIT_CODE"
}

exclude_null() {
	local key=$1 source=$2
	grep --invert-match '^null$' \
		|| err_exit "Could not find $key in $source"
}

raw() {
	local key=$1 json=$2 source=${3:-} reply
	reply=$(jq --raw-output "$key" <<< "$json") \
		|| err_exit "Could not parse json data in $source"

	if [[ -n ${source:-} ]]; then
		echo "$reply" | exclude_null "$key" "$source"

	elif [[ -n ${json:-} ]]; then
		echo "$reply"

	else
		warn "Empty response"
	fi
}

get_config() {
	yq . "$TRD_CONFIG"
}

get_rss_items() {
	local rss=$1 feed_url=$2
	xq --compact-output '.rss.channel.item[]' <<< "$rss" \
		| exclude_null feed "rss response from $feed_url"
}

get_directory() {
	local directory=".options.directories.$1" config_json=$2
	raw "$directory" "$config_json" "$TRD_CONFIG"
}

get_feeds() {
	local feeds_url=".options.feeds[].$1" config_json=$2
	raw "$feeds_url" "$config_json" "$TRD_CONFIG"
}

get_search_replace() {
	local search_replace='.options.search_replace | join(";")'
	raw "$search_replace" "$config_json" "$TRD_CONFIG"
}

get_matches() {
	local matches=".matches[]" config_json=$1
	jq --compact-output "$matches" <<< "$config_json" \
		| exclude_null "$matches" "$TRD_CONFIG"
}

process_feeds() {
	local config_json=$1
	while IFS= read -r feed_url; do
		rss=$(curl --fail --silent "$feed_url") \
			|| err_exit "Curl failed to fetch $feed_url with exit code $?"
		process_matches "$rss" "$config_json" "$feed_url"
	done < <(get_feeds url "$config_json")
}

process_matches() {
	local rss=$1 config_json=$2 feed_url=$3
	while IFS= read -r match; do
		label=$(raw '.label' "$match")
		names=$(raw '.names | join("|")' "$match")
		includes=$(raw '.includes | join("|")' "$match")
		releases=$(raw '.releases | join("|")' "$match")

		process_items "$label" "$names" "$includes" "$releases" "$rss" "$feed_url"
	done < <(get_matches "$config_json")
}

should_i_download_even_if() { # episode_already_exists
	local file=$1 includes=$2
	local file_release torrent_release file_include torrent_include
	[[ -n $1 ]] || return

	file_release=${episode_already_exists/*-/}
	file_release=${file_release/\.*/}
	file_release=${file_release,,}

	torrent_release=${title/*-/}
	torrent_release=${torrent_release,,}

	file_include=$(grep --extended-regexp --ignore-case --only-matching "$includes" <<< "${file,,}")
	torrent_include=$(grep --extended-regexp --ignore-case --only-matching "$includes" <<< "${title,,}")

	[[ $file_release == "$torrent_release" ]] \
		&& [[ $file_include == "$torrent_include" ]] \
		&& [[ $title =~ (PROPER|REPACK) ]] \
		&& return

	[[ ${releases/$file_release*/} =~ $torrent_release ]] \
		|| [[ ${includes/$file_include*/} ]] \
		&& return 1
}

get_episode() {
	local title=$1 episode
	grep \
		--extended-regexp \
		--ignore-case \
		--only-matching \
		--max-count=1 \
		's[0-9]{2}e[0-9]{2}|[0-9]{4}(\.[0-9]{2}){2}' \
		<<< "$title"
}

get_show_name() {
	local label=$1 file_title=$2 episode=$3 show_name=${2/\.$3*/}
	{
		find \
			"$content_dir/$label" \
			-maxdepth 1 \
			-type d \
			-iname "$show_name" \
			-printf '%f' \
			-quit \
			| grep '\S'
	} 2>/dev/null \
		|| echo "$show_name"
}

get_existing_episode() {
	local label=$1 show_name=$2 episode=$3
	find \
		"$content_dir/$label/$show_name" \
		"$watch_dir" \
		"$download_dir/$label" \
		-type f \
		-regextype egrep \
		-iregex ".*$show_name.?$episode.*" \
		-printf '%f' \
		-quit \
		| grep '\S'
}

should_i_process_item() {
	local match=$1 includes=$2 releases=$3 title=$4
	grep \
		--quiet \
		--extended-regexp \
		--ignore-case \
		"($match).*($includes).*($releases)" \
		<<< "$title"
}

process_items() {
	local label=$1 names=$2 includes=$3 releases=$4 rss=$5 feed_url=$6
	local title file_title episode show_name episode_already_exists link
	while IFS= read -r item; do
		title=$(raw '.title' "$item")

		should_i_process_item "$names" "$includes" "$releases" "$title" || continue

		log "Found a match --> $title"

		file_title=$(sed --regexp-extended "$search_replace" <<< "$title")

		episode=$(get_episode "$title") || {
			warn "Episode information could not be found for $title, skipping."
			continue
		}

		if
			show_name=$(get_show_name "$label" "$file_title" "$episode") \
				&& episode_already_exists=$(get_existing_episode "$label" "$show_name" "$episode")
		then
			should_i_download_even_if "$episode_already_exists" "$includes" || continue
		fi

		link=$(raw '.link' "$item")

		mkdir --parents "$watch_dir/$label"
		download_path="$watch_dir/$label/$file_title.torrent"
		curl --silent "$link" --output "$download_path"
		log "$title has been downloaded"
	done < <(get_rss_items "$rss" "$feed_url")
}

main() {
	config_json=$(get_config)

	content_dir=$(get_directory content "$config_json")
	watch_dir=$(get_directory watch "$config_json")
	download_dir=$(get_directory download_dir "$config_json")
	search_replace=$(get_search_replace "$config_json")

	if [[ -n ${1:-} ]] && [[ -f $1 ]]; then
		test_data_file=$1
		content_dir=tmp_test/content
		watch_dir=tmp_test/watch
		download_dir=tmp_test/download
		mkdir --parents "$content_dir" "$watch_dir" "$download_dir"
		process_matches "$(< "$test_data_file")" "$config_json" "$feed_url"
		exit
	fi

	process_feeds "$config_json"
}

[[ -f $TRD_CONFIG ]] || err_exit "Could not find config file: $TRD_CONFIG"

main "$@"
