#!/bin/bash

script_dir=$(realpath $(dirname "$(which $(basename "$0") || echo $0)"))
. "${script_dir}/nodered-env"
_check_if_already_running || exit 0

set -eo pipefail

_date() { LC_ALL=sv_SE.utf8 date "$@" ;}

# parse config file options
cfg=$(realpath "${script_dir}/configs/${script%.sh}.conf")
_cfg_get() {
	local tmp=$(sed 's/^\(\S\+\)\s*=\*/\1=/' "${cfg}")
	case ${1} in
		tgt) sed -n "s/target:folder=//p" <<<"${tmp}" ;;
		tmp) sed -n "s/temp:folder=//p" <<<"${tmp}" ;;
		log) sed -n "s/dl:log=//p" <<<"${tmp}" ;;
		rss) printf "$(sed -n "s/rss:${channel}=//p" <<<"${tmp}")" "${rss_str}" ;;
		aim) sed 's/\s*#.*//;/^\S\+:\S\+\s*=/d;/^$/d' <<<"${tmp}" ;;
	esac
}

# get/set paths
tgtbase=$(_cfg_get tgt)
tempdir="$(_cfg_get tmp)/tmp_${script%.sh}_${RANDOM}${RANDOM}"
log=$(_cfg_get log); touch ${log}

trap 'rm -rf "${tempdir}" "${csv_content}" 2>/dev/null' EXIT SIGINT SIGTERM

# replace whitespace with dots, and replace some special chars
_dotit() { sed 's/ - /-/g;s/,//;s/[ :]/./g' <<<"${1}" ;}

# the following functions should be rather self-explanatory, besides some wonky if cases that are mostly optional
_season() {
	if [[ ${dlurl} =~ sasong- ]]; then
		local tmp=$(grep -om1 '^[0-9]\+' <<<${dlurl/*sasong-})
	elif [[ ${desc} =~ Serie\ [0-9] ]]; then
		local tmp=$(sed 's/.*Serie \([0-9]\+\).*/\1/' <<<${desc})
	else
		local tmp=$(_date +'%y%m' -d"${ts}")
	fi
	printf 'S%02d' $((10#${tmp}))
}
_episode() {
	if [[ ${title} =~ [Aa]vsnitt.*[0-9]$ ]]; then
		local tmp=$(grep -om1 '[0-9]\+$' <<<${title})
	elif [[ ${dlurl} =~ avsnitt- ]]; then
		local tmp=$(grep -om1 '^[0-9]\+' <<<${dlurl/*avsnitt-})
	elif [[ ${desc} =~ Del" "[0-9]+" "av ]]; then
		local tmp=$(sed -E 's/.*Del ([0-9]+) av .*/\1/' <<<${desc})
	elif [[ ${title} =~ ^[0-9]+\. ]]; then
		local tmp=$(grep -om1 '^[0-9]\+' <<<${title})
	elif [[ $S =~ ^S[0-9]{4}$ ]]; then
		local tmp=$(_date +'%d' -d"${ts}")
	elif [[ ${title} =~ [åÅ]rskrönika ]]; then
		local tmp=00
	else
		local tmpx=$(ls "${tgtbase}/$(_dotit "${show}")/${S}" 2>/dev/null | grep -o "S[0-9]\+E99[0-9]\+" | sort -u | wc -l)
		local tmp=$(printf '99%02d' $((tmpx+1)))
	fi
	printf 'E%02d' $((10#${tmp}))
}
_filename() {
	if [[ ${title} =~ ^(Mån|Tis|Ons|Tor|Fre|Lör|Sön|Idag|Igår|Ikväll) ]]; then
		local tmp="$(_date +"%a %d %b" -d"${ts}") ${title/* }"
	elif [[ ${title} =~ ^[0-9]+\. ]]; then
		local tmp="$(sed 's/^[0-9]\+\.\s*//' <<<${title})"
	else
		local tmp=${title}
	fi
	_dotit "${show} ${S}${E} ${tmp}"
}
_make_mkv() {
	cd "${tempdir}" || { echo "shit hit the fan" ; exit 1 ;}
	local f=$(ls *.mp4)
	local l=$(ffprobe "${f}" 2>&1 | awk -F'[()]' '/Stream #0.*Audio/{ print $2 }')
	local s=$(ls *.srt)
	fn="${f%.*}.mkv"

	if [[ -n $s ]]; then
		args="-map 0 -map 1 -c copy -metadata:s:s:0 language=${l:-swe} -metadata:s:s:1 language=swe"
		ffmpeg -i "${f}" -i "${s}" ${args} "${fn}" && rm "${f}" "${s}"
		local es=$?
	fi

	if [[ -z ${s} || ${es} != 0 ]]; then
		args="-map 0 -c copy -metadata:s:s:0 language=${l:-swe}"
		ffmpeg -i "${f}" ${args} "${fn}" &>/dev/null && rm "${f}"
	fi
	[[ $? == 0 ]] && mv "${fn}" "${tgtdir}"
}

# 4th and 5th fields are optional in the config file, only list titles that matches or excludes them (case insensitive).
csv_content=$(mktemp)
while IFS=, read channel rss_str filter exclude; do
	rss_url=$(_cfg_get rss)
	rss_processed=$(curl -s "${rss_url}" | sed -En '/title>/,${s/amp;//;s,\s*</?(title|link|pubDate|description)>\s*(SVT Play -\s+)?,,gp}')
	show=$(sed '1!d;s/\&/\\\&/g' <<<${rss_processed})

	# make a csv out of the rest. (replace newline with delimiter, make a new newline every fourth delimiter, pass through optional filters, add show name as first field in csv)
	[[ -n $(sed '4!d' <<<${rss_processed}) ]] && sed '1,3d' <<<${rss_processed} \
			| tr '\n' '|' \
			| sed 's/|/\n/4;P;D' \
			| egrep -- "${filter:-.}" \
			| egrep -v -- "${exclude:-✂✂}" \
			| sed "s/.*/${show}|&/" \
			>>${csv_content}
done < <(_cfg_get aim)

# awk/sed proved to be the most reliable option, while IFS=\| read did not deliver as expected
_a() { awk -F'|' "NR==1{print \$${1}}" ${csv_content} ;}
until [[ -z $(_a 0) ]]; do
	if ! grep -q "$(_a 3)" ${log}; then
		show=$(_a 1) ; title=$(_a 2) ; dlurl=$(_a 3) ; desc=$(_a 4) ; ts=$(_a 5)
		S=$(_season)
		E=$(_episode)
		fn=$(_filename)
		tgtdir="${tgtbase}/$(_dotit "${show}")/$S"
		mkdir -p "${tgtdir}"
		mkdir -p "${tempdir}"
		cd "${tempdir}"

		if svtplay-dl -S --force "${dlurl}" -o "${fn}" && _make_mkv; then
			pmscan "${tgtdir}"
			echo "${dlurl}" >>${log}
			echo "${script}: successfully downloaded ${show} ${S}${E}"
		else
			echo "${script}: failed to download ${dlurl}"
			rm -rf "${tempdir}"
		fi
		cd "${tgtbase}"
	fi

	# delete the first row so we can process the next line, if any
	sed -i '1d' ${csv_content}

done
