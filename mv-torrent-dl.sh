#!/bin/bash
# Author:      Kaffe (kaffemyers@gmail.com)
# Created:     2019-12-26
# Description: Shuffle media arround to the appropriate place
#              Tries to unify the naming scheme of files as well
# E.g.:
# From - "Game of Thrones 01x01 Winter Is Coming (1080p)-release.mkv"
# To   - "Game.of.Thrones.S01E01.Winter.Is.Coming.1080p-release.mkv"

script_dir=$(realpath $(dirname "$(which $(basename "$0") || echo $0)"))
. "${script_dir}/nodered-env"
_check_if_already_running || exit 0

# set some variables
cfg="$script_dir/configs/rss-torrent-dl.conf"
base_fr_dir=/storage/torrent-dl/finished
base_to_dir=/storage/stream
unset dirs_to_scan


_unrar() {
	cd "$1"
	while :; do
		unset rars
		while IFS= read -r -d $'\0' rar; do
			rars+=("$rar")
		done < <(find . -iname '*.rar' -print0)

		if [[ -n $rars ]]; then
			for rar in "${rars[@]}"; do
				unrar e -o+ "$rar"
			done

		else
			break
		fi
	done
}


_delete_empty_folders() { find "${fr_dir}" -mindepth 1 -empty -type d -delete ;}

_pipe_delimited() { tr ' ' '|' <<<${@} ;}


for section in Movies Series; do
	unset filelist
	fr_dir=${base_fr_dir}/${section}
	to_dir=${base_to_dir}/${section}
	_unrar "${fr_dir}"
	find "$fr_dir" -type d -iname 'sample' | xargs rm -rf
	find "$fr_dir" -regextype posix-egrep -regex '.*\.r(ar|[0-9]{2})$' -delete

	unwanted=$(_pipe_delimited nfo srr sfv jpg jpeg)
	find "${fr_dir}" -type f -regextype posix-egrep -regex ".*(${unwanted})$" -delete
	_delete_empty_folders

	if [[ $section == Movies ]]; then
		extensions=$(_pipe_delimited mkv mp4 avi srt)

		while IFS= read -r -d $'\0' movie; do
			unset moviescan
			base_name=$(basename "$movie")

			if [[ -d "$movie" ]]; then
				mv "$movie" "${to_dir}"
				moviescan="${to_dir}/${base_name}"

			elif [[ "${movie,,}" =~ \.(${extensions})$ ]]; then
				moviescan="${to_dir}/${base_name%\.*}" # extension check already made, so just removing "any" extension

				mkdir -p "$moviescan"
				mv "$movie" "$moviescan"

			else
				rm -f "$movie"
			fi

			[[ -n $moviescan ]] && dirs_to_scan+=("$moviescan")
		done < <(find "$fr_dir" -mindepth 1 -maxdepth 1 -print0)

		_delete_empty_folders
	fi
done

#### the rest is for Series section
# $to_dir and fr_dir remains from for loop above

# $rex used to find files with one of the standard file name structures
rex=(
	'[sS][0-9]+.?[eE]'
	'[0-9]+\s?[xX]\s?[0-9]+'
	'[0-9]{4}\.[0-9]{2}\.[0-9]{2}\.'
)
rex=$(_pipe_delimited "${rex[@]}")

# rename schemes to create a nice, standardized filename
rename=$(sed -E 's/^\s+//;s/#.*//;/^$/d;/label\s+=/,$d;1,/\[rename\]/d' "${cfg}")

# get list of files to work with and process
while IFS= read -r -d $'\0' file; do

	until (( $(date -r "$file" +%s) < $(date +%s -d'20 seconds ago' ) )); do sleep 5; done

	fn=$(basename "$file" | sed -r "$rename") # file name to-be
	if [[ "${file,,}" =~ s[0-9]+.?e[0-9] ]]; then
		printf -v season '%02d' $(sed 's/.*s0*\([0-9]\+\)e.*/\1/i' <<<$fn)
		printf -v ep '%02d' $(sed 's/.*s[0-9]\+e0*\([0-9]\+\).*/\1/i' <<<$fn)
		showname=$(sed 's/\.\+s[0-9]\+e[0-9]\+.*//i' <<<$fn)

		# case can vary, this avoids e.g. "Watchmen" and "watchmen" to co-exist
		tmp=$(find "$to_dir"/* -maxdepth 0 -type d -iname "$showname" -printf '%f')
		[[ -n $tmp ]] && showname="$tmp"

		# set destination directory and file name
		destination="$to_dir/$showname/S$season/$fn"
	else # like the.daily.show
		showname=$(sed 's/\(.*\)\.[0-9]\{4\}\.[0-9]\{2\}\.[0-9]\{2\}.*/\1/' <<<$fn)
		tmp=$(find "$to_dir"/* -maxdepth 0 -type d -iname "$showname" -printf '%f')
		[[ -n $tmp ]] && showname="$tmp"
		destination="$to_dir/$showname/$fn"
	fi

	# create destination directory if missing
	[[ -d "$(dirname "$destination")" ]] || mkdir -p "$(dirname "$destination")"

	# move to its final destination
	mv "$file" "$destination"
	dirs_to_scan+=("$(dirname "$destination")")
	echo "${script}: $(basename "${file}")"

done < <(find "$fr_dir" -type f -regextype posix-egrep -regex ".*($rex).*" -print0 | sort -z)

[[ -n ${dirs_to_scan} ]] && while IFS= read -r -d $'\0' scan_dir; do
	pmscan "${scan_dir}"
done < <(printf '%s\0' "${dirs_to_scan[@]}" | sort -zu)

_delete_empty_folders
