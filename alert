#!/bin/bash
# Author:      Kaffe (the@kaffemyers.com)
# Created:     2021-02-07
# Description: Fills logs and updates status to stdout
#              Made for use with nodered, but could be used with any notification system, really
#              id   = alarm code. It's an arbitrary number of 1-999 of your own choosing
#              hn   = hostname
#              ip   = ip
#              subj = if an id/host has more than one possible entry, this needs to be set,
#                     can otherwise be left empty. Will be first part of alarm message if set.
#              msg  = message to be output
#
# log example: 2021-02-11 19:39:14;002;New;espresso;10.11.12.4;/dev/sda1;Available: 9.862G  Use%: 12% /
# stdout ex:   New    espresso   (10.11.12.4): /dev/sda1 Available: 9.862G  Use%: 12% /   

unset t

[[ $1 == -t ]] && t=_test && shift
id=$( printf '%03d' $(( 10#$1 )) )
ip=$2
hn=$3
msg=$4
subj=$5

a_log=$a_log$t
h_log=$h_log$t
touch $a_log $h_log

_logmsg() {	
	case $1 in
		write) printf '%s;%s;%s;%s;%s;%s;%s\n' "$(date +%F\ %T)" $id $stat $ip $hn "$subj" "$msg" | tee -a $h_log ;;
		print) awk -F';' '{ printf "%-6s %-9s %13s: %s %s\n", $3, $5, "("$4")", $6, $7 }' ;;
		partlook) grep -m1 ";$id;\S\+;$ip;$hn;$subj;" $a_log ;;
		complook) grep -m1 ";$id;\S\+;$ip;$hn;$subj;$msg$" $a_log ;;
	esac
}

if [[ $msg != CLEAR ]]; then
	unset stat
	if ! store=$(_logmsg complook); then
		if store=$(_logmsg partlook); then
			export stat="Update"
		else
			export stat="New"
		fi
	fi

	if [[ -n $stat ]]; then
		[[ $stat == Update ]] && sed -i "/${store//\//\\\/}/d" $a_log
		_logmsg write | tee -a $a_log | _logmsg print
	fi

elif store=$(_logmsg partlook); then
	sed -i "/${store//\//\\\/}/d" $a_log
	subj=$(cut -d';' -f6 <<<$store)
	msg=$(sed 's/.*;//' <<<$store)
	stat=Clear
	_logmsg write | _logmsg print
fi

exit 0
