#!/bin/bash
# Author:      Kaffe (kaffemyers@gmail.com)
# Created:     2021-02-19

unset found
search=(bamse)
exclude=("^bamse-?(1981|1966|tvillingarna-bob|oppet-arkiv)?$")
playcfg=/home/nodered/bin/configs/playdl.conf

for s in "${search[@]}"; do
	while read hit; do
		grep -q "svt,${tmp}" ${playcfg} || echo "svt,${tmp}" >> ${playcfg}
	done < <(curl -s "https://www.svtplay.se/sok?q=$s" \
		| sed 's/"slug/\n&/g;s/image\\/\n/g' \
		| sed '/^"slug/!d;s/^.slug..:..//;s/\\.*//' \
		| egrep -v "${exclude}")
done
