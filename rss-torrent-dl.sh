#!/bin/bash
# Author:      Kaffe (simon@wagemyr.se)
# Created:     2020-04-04
# Description: Download torrent files from rss feed

unset entry
IFS=$'\n'

# remove comments/crap from config file and store in a variable for further processing
cfg_f="$(realpath "$(dirname "$0")/configs/$(basename "${0%.sh}").conf")"
conf=$(sed 's/#.*//;s/^\s*//;s/\s*$//;s/\s*=\s*/=/;/^$/d' "$cfg_f")

# rename schemes in sed -r format
rename=$(sed -n '/\[rename\]/,/^\(\[\|label\)/{/^\(\[\|label\)/d;p}' <<<$conf)

# just a dummy var that shouldn't match anything
nm=$(</dev/urandom tr -cd 'a-z0-9' | head -c120)

log="/storage/log/$(basename "$0")_stdout.log"

# Get stuff from config
_pick() { while IFS= read val; do echo "${val#*=}"; done < <(grep "^$1" <<<$conf) ;}
cont_d=$(_pick content.dir)
ctrl_d=$(_pick ctrl.dir)
watch_d=$(_pick watch.dir)
dl_d=$(_pick dl.dir)

# Get any new entries in feed(s)
for f in $(sed -n 's/^\(\S.*\)\.feed.*/\1/p' <<<$conf); do
	old="$ctrl_d/feed_$f.html"
	feed=$(curl -s "$(_pick $f.feed)" 2>/dev/null | sed '/<item>/,$!d')
	if [[ -f "$old" ]]; then
		new=$(diff "$old" <(cat <<<$feed) | sed -n '/\[Category:/d;s/^> //p')
	else
		new=$feed
	fi
	# store recent feed for processing next time around
	cat <<<$feed >"$old"

	# carve out just the title and the link
	[[ -n $new ]] && entry+=$(awk '/<(title|link)>/{
		sub(/.*<(title|link)>/,"")
		sub(/<.(title|link)>/,"")
		gsub(/<!.CDATA.|]]>|amp;/,"")
		if($0 ~ /^http/) print
		else printf "%s|", $0
	}' <<<$new)$'\n'
done

conf=$(sed -n '/^label=/,$p' <<<$conf)
sed -n 's/|.*//p' <<<$entry >> "$log"

# extract what to look for in config
[[ -n $entry ]] && for label in $(_pick label= <<<$conf); do
	tmp_conf=$(sed -n '1d;/^label=/,$p' <<<$conf)
	conf=$(sed '1d;/^label=/,$d' <<<$conf)
	match=$(awk '!/=/{ m=m"|"$0 } END{gsub(/'\''/,"&?",m);sub(/\|/,"",m); print m}' <<<$conf)
	include=$(_pick include= | sed 's/\s*,\s*/|/g')
	release=$(_pick release= | sed 's/\s*,\s*/|/g')
	conf=$tmp_conf

	for e in $(egrep -i "(${match:-.?}).*(${include:-.?}).*(${release:-.?})" <<<$entry); do
		title=${e/|*}
		echo -n "MATCH: $title -> " >> "$log"
		[[ -n $rename ]] && title=$(sed -r "$rename" <<<$title)
		echo "$title" >> "$log"
		link=${e/*|}
		dl=1
		ep=$(egrep -iom1 '(s[0-9]{2}e[0-9]{2}|[0-9]{4}(\.[0-9]{2}){2})' <<<$title)
		name=$(find $cont_d/$label -maxdepth 1 -type d -iname "${title/\.${ep:-$nm}*}*" -printf '%f' -quit | grep '.')
		if file=$(find "$cont_d/$label/${name:-$nm}" "$watch_d" "$dl_d/$label" -type f -regextype egrep -iregex ".*$name.?$ep.*" -printf '%f' -quit | grep '.'); then
			file_rel=$(sed 's/.*-//;s/\..*//' <<<${file,,})
			torrent_rel=$(sed 's/.*-//' <<<${title,,})
			file_inc=$(egrep -io "$include" <<<${file,,})
			torrent_inc=$(egrep -io "$include" <<<${title,,})

			# check for PROPER/REPACK release, download if so
			if [[ $file_rel == $torrent_rel && $file_inc == $torrent_inc && $title =~ (PROPER|REPACK) ]]; then
				: # skip other checks
			else
				# only download if prioritized release group and not already the same
				[[ ${release/$file_rel*} =~ $torrent_rel || ${release:-$nm} != "$nm" ]] && unset dl
				# only download if prioritized include and not already the same
				[[ ${include/$file_inc*} =~ $torrent_inc || ${include:-$nm} != "$nm" ]] && unset dl
			fi
		fi

		if [[ -n $dl ]]; then
			[[ ! -d "$watch_d/$label" ]] && mkdir -p "$watch_d/$label"
			curl -s "$link" -o "$watch_d/$label/$title.torrent"
			echo "$(basename "$0"): $title"
		fi
	done
done
exit 0
