#!/bin/bash
set -eo pipefail

script=$(basename $0)
sc_dir=$(dirname $0)

# only continue if an instance isn't already running
ps aux | sed -n "/$script$/q1" || exit

cfg=$(realpath "${sc_dir}/configs/${script%sh}conf"
log=/storage/log/${script%sh}log; touch $log
tgtbase=/storage/stream/Play

# 4th field is optional in the config file, only list titles that matches it (case insensitive).
while IFS=, read chan show rss spec; do
	while read dlurl; do
		grep -q "$dlurl" $log || list+=("$chan,$show,$dlurl")
	done < <(curl -s "$rss" | sed -En "1,8d;/title>.*(${spec:-.*})/I{n;s,\s*</?link>\s*,,gp}")
done < <(sed 's/\s*#.*//;/^$/d' "$cfg")

while IFS=, read chan show dlurl; do
	tgtdir="$tgtbase/$show"
	mkdir -p "$tgtdir" && cd "$tgtdir" || { echo "Could not access $tgtdir"; exit 1 ;}

	if svtplay-dl -S --force "$dlurl"; then
		echo "$dlurl" >>$log
		echo "$script: successfully downloaded ${dlurl/*\/}"
	else
		echo "$script: failed to download $dlurl"
	fi
done < <(printf '%s\n' "${list[@]}")

